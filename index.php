<?php

require_once __DIR__.'/bootstrap.php';

use Freelancehunt\Models\Project;
use Freelancehunt\Models\Skill;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

$loader = new FilesystemLoader(__DIR__.'/views');
$twig = new Environment($loader, [
    'cache' => __DIR__.'/cache/compilation_cache',
]);

// hardcode pagination, refactor
$projects = Project::select([
    'id',
    'name',
    'budget',
    'employer_name',
    'employer_login',
    'link',
    'published_at'
])
    ->whereHas('skills', function($query){
        return $query->whereIn('id', [99, 86, 1]);
    })
    ->when(isset($_GET['page']) && intval($_GET['page'] > 0), function($query) {
        return $query->skip(($_GET['page'] * 10)+1);
    })
    ->take(11)
    ->with(['skills'])
    ->get();

$skills = Skill::withCount('projects')
    ->orderByDesc('projects_count')
    ->get()
    ->filter(fn($skill) => $skill->projects_count > 0);

$chart = Project::selectRaw('SUM(CASE WHEN projects.budget < 500 AND projects.budget > 0 THEN 1 ELSE 0 END) as lt500, SUM(CASE WHEN projects.budget >= 500 AND projects.budget < 1000 THEN 1 ELSE 0 END) as lt1000, SUM(CASE WHEN projects.budget >= 1000 AND projects.budget < 5000 THEN 1 ELSE 0 END) as lt5000, SUM(CASE WHEN projects.budget >= 5000 AND projects.budget < 10000 THEN 1 ELSE 0 END) as lt1000, SUM(CASE WHEN projects.budget >= 10000 THEN 1 ELSE 0 END) as gt10000')
    ->get()
    ->values();

echo $twig->render('index.twig', [
    'title' => 'Project List',
    'projects' => $projects,
    'skills' => $skills,
    'chart' => $chart[0],
    'page' => isset($_GET['page']) ? $_GET['page'] : 1
]);
