<?php

use Freelancehunt\Migration;

class ProjectSkill extends Migration
{
    public function up()
    {
        $this->schema->create('project_skill', function (Illuminate\Database\Schema\Blueprint $table) {
            $table->unsignedInteger('project_id');
            $table->unsignedInteger('skill_id');

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('skill_id')->references('id')->on('skills')->onDelete('cascade');
        });
    }

    public function down()
    {
        $this->schema->table('project_skill', function (Illuminate\Database\Schema\Blueprint $table) {
            $table->dropForeign(['project_id']);
            $table->dropForeign(['skill_id']);
        });
        $this->schema->drop('project_skill');
    }
}
