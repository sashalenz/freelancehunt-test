<?php

use Freelancehunt\Migration;

class Skill extends Migration
{
    public function up()
    {
        $this->schema->create('skills', function (Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        $this->schema->drop('skills');
    }
}
