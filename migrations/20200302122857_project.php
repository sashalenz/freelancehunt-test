<?php

use Freelancehunt\Migration;

class Project extends Migration
{
    public function up()
    {
        $this->schema->create('projects', function (Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('budget')->nullable();
            $table->string('employer_name')->nullable();
            $table->string('employer_login')->nullable();
            $table->string('link')->nullable();
            $table->datetime('published_at');
            $table->nullableTimestamps();
        });
    }

    public function down() {
        $this->schema->drop('projects');
    }
}
