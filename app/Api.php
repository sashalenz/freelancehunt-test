<?php

namespace Freelancehunt;

use GuzzleHttp\Client;

class Api
{
    const API_BASE = 'https://api.freelancehunt.com/v2/';
    const API_LOCALE = 'ru';
    private $api_secret;
    private $client;

    /**
     * Api constructor.
     * @param $api_secret
     */
    public function __construct($api_secret)
    {
        $this->api_secret = $api_secret;

        $this->client = new Client([
            'base_uri' => self::API_BASE,
            'headers' => [
                'Authorization' => 'Bearer ' . $this->api_secret,
                'Accept-Language' => self::API_LOCALE
            ],
        ]);
    }

    /**
     * @param $page
     * @return string
     */
    public function getProjects($page)
    {
        try {
            $request = $this->client->get('projects', [
                'query' => [
                    'page[number]' => $page
                ]
            ]);
            $response = json_decode($request->getBody()->getContents(),1);
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return $response['data'];
    }

    /**
     * @return string
     */
    public function getSkills()
    {
        try {
            $request = $this->client->get('skills');
            $response = json_decode($request->getBody()->getContents(),1);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $response['data'];
    }
}
