<?php

namespace Freelancehunt\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Project extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'budget',
        'employer_name',
        'employer_login',
        'link',
        'published_at'
    ];

    protected $dates = [
        'published_at'
    ];

    public $incrementing = false;

    /**
     * @return BelongsToMany
     */
    public function skills() : BelongsToMany
    {
        return $this->belongsToMany(Skill::class);

    }
}
