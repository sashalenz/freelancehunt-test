<?php

namespace Freelancehunt\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Skill extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name'
    ];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @return BelongsToMany
     */
    public function projects() : BelongsToMany
    {
        return $this->belongsToMany(Project::class);

    }
}
