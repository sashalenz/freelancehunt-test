<?php

namespace Freelancehunt;

use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class PB_API
{
    const URL = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';

    /**
     * @var Client
     */
    private Client $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => self::URL,
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
            ]
        ]);
    }

    /**
     * @return mixed
     */
    public function getCurrencies()
    {
        $data = [];
        try {
            $request = $this->client->get('');
            $response = json_decode($request->getBody()->getContents(),1);
            foreach($response as $curr) {
                $data[$curr['ccy']] = $curr['buy'];
            }
        } catch (\Exception $e){
//
        }
        return $data;
    }
}
