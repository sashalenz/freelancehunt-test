<?php

namespace Freelancehunt\Console;

use Freelancehunt\Api;
use Freelancehunt\Models\Skill;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FetchSkills extends Command
{
    protected static $defaultName = 'skills:fetch';

    protected function configure()
    {
//help here
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $api = new Api($_ENV['API_TOKEN']);
        $skills = $api->getSkills();

        foreach($skills as $data) {
            if(Skill::find($data['id'])) {
                $output->writeln('found '.$data['id']);
                continue;
            }

            Skill::create([
                'id' => $data['id'],
                'name' => $data['name']
            ]);
        }
        return 0;
    }
}
