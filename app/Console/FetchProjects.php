<?php

namespace Freelancehunt\Console;

use Carbon\Carbon;
use Freelancehunt\Api;
use Freelancehunt\Models\Project;
use Freelancehunt\Models\Skill;
use Freelancehunt\PB_API;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FetchProjects extends Command
{
    protected static $defaultName = 'projects:fetch';

    protected function configure()
    {
//help here
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $api = new Api($_ENV['API_TOKEN']);

        $pb_api = new PB_API();
        $currencies = $pb_api->getCurrencies();

        $page = 1;
        while(true) {
            $projects = $api->getProjects($page);

            if (!is_array($projects) || count($projects) === 0) {
                break;
            }
            $output->writeln(count($projects));

            foreach($projects as $data) {

                if (Project::find($data['id'])) {
                    $output->writeln('found '.$data['id']);
                    continue;
                }

                if (isset($data['attributes']['budget'])) {
                    if ($data['attributes']['budget']['currency'] !== 'UAH') {
                        $rate = $currencies[$this->checkCurrencyName($data['attributes']['budget']['currency'])];
                        if(!$rate) {
                            $output->writeln(print_r($currencies,1));
                            $output->writeln($this->checkCurrencyName($data['attributes']['budget']['currency']));
                        }
                        $budget = $data['attributes']['budget']['amount'] * $rate;
                    } else {
                        $budget = $data['attributes']['budget']['amount'];
                    }
                }

                $project = Project::create([
                    'id' => $data['id'],
                    'name' => $data['attributes']['name'],
                    'budget' => $budget ?? 0,
                    'employer_name' => !empty($data['attributes']['employer']) ? implode(' ', [$data['attributes']['employer']['first_name'],$data['attributes']['employer']['last_name']]) : null,
                    'employer_login' => !empty($data['attributes']['employer']) ? $data['attributes']['employer']['login'] : null,
                    'link' => $data['links']['self']['web'],
                    'published_at' => Carbon::parse($data['attributes']['published_at']),
                ]);
                $skills = Skill::whereIn('id', array_column($data['attributes']['skills'], 'id'))->get();
                $project->skills()->attach($skills);
            }

            $page++;
        }
        return 0;
    }

    /**
     *
     * check if currency not in list
     *
     * @param $code
     * @return mixed
     */
    protected function checkCurrencyName($code)
    {
        $codes = [
            'RUB' => 'RUR'
        ];
        return $codes[$code] ?? $code;
    }
}
