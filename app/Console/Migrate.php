<?php

namespace Freelancehunt\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Migrate extends Command
{
    protected static $defaultName = 'migrate';

    protected function configure()
    {
//help here
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once '../../migrations/Project.php';
        require_once '../../migrations/Skill.php';
        require_once '../../migrations/ProjectSkill.php';
        return 0;
    }
}
