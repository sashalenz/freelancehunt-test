# test app

## requirements
PHP7.4, Composer, MySQL 5.6 with PDO module

## installation
Install dependencies

```$xslt
composer install
```

Configure .env. Copy example file from .env.example
```$xslt
PROJECT_NAME=
API_TOKEN=

DB_HOST=
DB_NAME=
DB_USER=
DB_PASS=
```

Run database migrations
```$xslt
php vendor/bin/phinx migrate -c config-phinx.php
```

Run PHP Server

Done.

## usage

Available console commands

Fetch Skills (fun first)
```$xslt
php console.php skills:fetch
```

Fetch Projects
```$xslt
php console.php projects:fetch
```

## tests

todo



