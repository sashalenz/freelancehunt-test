<?php

require __DIR__.'/bootstrap.php';

use Freelancehunt\Console\FetchProjects;
use Freelancehunt\Console\FetchSkills;
use Freelancehunt\Console\Migrate;
use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new Migrate());
$application->add(new FetchProjects());
$application->add(new FetchSkills());

$application->run();
